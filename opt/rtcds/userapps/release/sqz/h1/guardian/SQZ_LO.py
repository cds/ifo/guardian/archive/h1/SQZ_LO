# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_LO.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/h1/guardian/SQZ_LO.py $

import sys
import time
from guardian import GuardState, GuardStateDecorator
import cdsutils as cdu

nominal = 'LOCKED_OMC'

######FUNCTIONS######

def LO_LOCKED():
    log('In LO_LOCKED()')
    flag = False
    RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True)
    if RFave[0] > -5 and RFave[1] < 0.1:
        flag = True
    return flag

def EOM_OK():
    flag = False
    if abs(ezca['SQZ-FIBR_SERVO_EOMRMSMON']) < 4:
        flag = True
    return flag

    return abs(ezca['SQZ-FIBR_SERVO_EOMRMSMON']) < 4 

def OPO_LOCKED():
    #log('Checking OPO in OPO_LOCKED()')
    flag = False
    #LOCKED_CLF_DUAL
    if ezca['GRD-SQZ_OPO_STATE_N'] == 17:
        flag = True
    #LOCKED_SEED_HIGH
    if ezca['GRD-SQZ_OPO_STATE_N'] == 27:
        flag = True
    #LOCKED_SEED_LOW
    if ezca['GRD-SQZ_OPO_STATE_N'] == 37:
        flag = True
    #LOCKED on any
    if ezca['GRD-SQZ_OPO_STATE_N'] == 10:
        flag = True
    return flag

def IMC_LOCKED():
    #log('Checking IMC in IMC_LOCKED()')
    flag = False
    if ezca['GRD-IMC_LOCK_STATE_N'] == 100 or ezca['GRD-IMC_LOCK_STATE_N'] == 70:
        flag = True
    if ezca['GRD-IMC_LOCK_STATE_N'] == 55:
        flag = True  
    return flag


def CLF_LOCKED():
    log('Checking CLF in CLF_LOCKED()')
    flag = False
    if ezca['GRD-SQZ_CLF_STATE_N'] == 10:
        flag = True
    return flag


#####################

class INIT(GuardState):
    index = 0

    def main(self):
        return True

#        if LO_locked():
#            return 'LOCKED'
#        else:
#            return 'DOWN'


class DOWN(GuardState):
    index = 1
    goto = True

    def main(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        #ezca['SQZ-HD_FLIPPER_CONTROL'] = 0 #close HD flipper
        ezca['SQZ-LO_SERVO_COMBOOST'] = 0
        ezca['SQZ-LO_SERVO_IN2EN'] = 0
        ezca['SQZ-LO_SERVO_IN1EN'] = 0
        ezca['SQZ-LO_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-LO_SERVO_SLOWCOMP'] = 0
        ezca['SQZ-LO_SERVO_SLOWFILTER'] = 0
        ezca['SQZ-LO_SERVO_FASTEN'] = 1
        ezca['SQZ-LO_SERVO_SLOWOPT'] = 1
        return True

    def run(self):
        return True

class IDLE(GuardState):
    index = 3
    request = False

    def run(self):
        if not OPO_LOCKED() or not IMC_LOCKED():
            notify('OPO or IMC unlocked')
        else:
            return 'ADJUST_FREQUENCY'

#    def run(self):
#        if ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] < -30:
#            log('Check 3MHz RF level')
#        if ezca['GRD-SQZ_PLL_STATE_N'] != 1:
#            log('PLL not DOWN')
#        if abs(ezca['SQZ-LO_SERVO_FASTMON']) == 10:
#            log('LO rails')
#            ezca['SQZ-LO_SERVO_COMBOOST']=0
#        return (ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] > -30) and (ezca['GRD-SQZ_PLL_STATE_N'] == 1) and (abs(ezca['SQZ-LO_SERVO_FASTMON']) < 10)


class ADJUST_FREQUENCY(GuardState):
    index = 4
    request = True

    def main(self):
        '''self.delta_t = 1/16.
        self.ugf = 0.3
        self.cal = 7e-8  #Volt per Hz OPO PZT
        #self.counter = 0'''
        servo_gain = -2.1e-8 
        control_chan = 'SQZ-OPO_PZT_1_OFFSET'
        readback_chan = 'SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO'
        self.servo = cdu.Servo(ezca, control_chan, readback=readback_chan,
                               gain=servo_gain)

    def run(self):
        #if not OPO_LOCKED():
        #    return 'IDLE'
        if not IMC_LOCKED():
            return 'IDLE'
        
        #self.counter += 1
        #log('Counter = {}'.format(self.counter))
        self.servo.step() 
        '''err = ezca['SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO'] #ezca['SQZ-FREQ_SQZBEAT'] - 2*ezca['SQZ-FREQ_PSLVCO']
        self.cntrl = self.delta_t*self.ugf*err
        log('err = {}'.format(err))
        ezca['SQZ-OPO_PZT_1_OFFSET'] += self.cal*self.cntrl'''

        if abs(ezca['SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO']) < 50000: 
            return True
   
################################################################################################
#LOCKING 3MHz with Homodyne
################################################################################################

#NOT READY, DON'T GO HERE
class LOCKING_HD(GuardState):
    index = 5
    request = False

    def main(self):
        log('locking LO Homodyne')
            
    def run(self):

        if not OPO_LOCKED():
            return 'IDLE'
        if not IMC_LOCKED():
            return 'IDLE'
        if ezca['GRD-SQZ_CLF_STATE_N'] != 10:
            log('CLF not locked')       
        if ezca['GRD-SQZ_OPO_STATE_N'] == 18:
           log('EOM rails')        
        
        self.servo.step() 

        if abs(ezca['SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO']) < 100000: 
            ezca['SQZ-LO_SERVO_IN2EN'] = 1


        if RFave[0] > -10: #Beatnote level OK
            ezca['SQZ-LO_SERVO_IN2EN'] = 1 #close the loop
            RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True)
            #if loop closed and stdev OK, move on
            if RFave[1] < 0.1:
                return True
            elif ezca['GRD-SQZ_OPO_STATE_N'] == 17:
                notify('LO loop closed but noisy, EOM not railing')
                return True
            elif ezca['GRD-SQZ_OPO_STATE_N'] == 18:
                log('EOM rails')
            else:
                notify('LO LOCKED condition not met somehow, not returning True')
        else:
            notify('Waiting for good beatnote')
            log('Waiting for good beatnote')


class LOCKED_HD(GuardState):
    index = 10

    def run(self):

        RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True)
        log('RF3 average 1s = {}'.format(RFave[0]))
        log('RF3 std 1s = {}'.format(RFave[1]))

        ezca['SQZ-LO_SERVO_COMBOOST']=1
        ezca['SQZ-LO_SERVO_SLOWBOOST']=1

        if ezca['GRD-SQZ_OPO_STATE_N'] == 18:
           log('EOM rails')
           notify('EOM rails')
           #if using OMC, close shutter

        if not OPO_LOCKED:
           log('OPO not nominal, waiting for OPO to lock on CLF DUAL')
           notify('OPO not nominal, waiting for OPO to lock on CLF DUAL')
           return 'LOCKING_HD'

        if not IMC_LOCKED:
           log('IMC not nominal, check IFO status')
           notify('IMC not nominal, check IFO status')
           return 'LOCKING_HD'

        if not CLF_LOCKED:
           log('CLF not nominal, check CLF status')
           notify('CLF not nominal, check CLF status')
           return 'LOCKING_HD'
 
        if abs(ezca['SQZ-LO_SERVO_FASTMON']) == 10 or abs(ezca['SQZ-LO_SERVO_SLOWMON'])==10:
           log('LO rails')
           notify('LO rails')
           return 'LOCKING_HD'

        if RFave[0] > -5:
            log('LO LOCKED')
            return True
        else:
            log('RF3 dB level low, relocking')
            return 'LOCKING_HD'


################################################################################################
#LOCKING 3MHz with OMC
################################################################################################


class LOCKING_OMC(GuardState):
    index = 15
    request = False

    def main(self):
        #ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        ezca['SQZ-LO_SERVO_COMBOOST'] = 0
        ezca['SQZ-LO_SERVO_IN2EN'] = 0
        ezca['SQZ-LO_SERVO_IN1EN'] = 0
        ezca['SQZ-LO_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-LO_SERVO_SLOWFILTER'] = 0
        
        servo_gain = -2.1e-8
        control_chan = 'SQZ-OPO_PZT_1_OFFSET'
        readback_chan = 'SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO'
        self.servo = cdu.Servo(ezca, control_chan, readback=readback_chan,
                               gain=servo_gain)

        log('locking LO OMC')
        self.fail_flag = False
        self.counter = 0

    def run(self):
        if not self.fail_flag:        
            if not OPO_LOCKED():
                return 'IDLE'
            if not IMC_LOCKED():
                return 'IDLE'
            if ezca['GRD-SQZ_CLF_STATE_N'] != 10:
                log('CLF not locked')       
            if ezca['GRD-SQZ_OPO_STATE_N'] == 18:
               log('EOM rails')        
            
            self.servo.step()
        
            if self.counter ==0 and abs(ezca['SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO']) < 50000: 
                ezca['SYS-MOTION_C_BDIV_C_OPEN'] = 1
                self.counter +=1
            if self.counter == 1 and ezca['SYS-MOTION_C_BDIV_C_POSITION'] == 0: # if the beam diverter is open
                if ezca['SQZ-OMC_TRANS_RF3_DEMOD_RFMON'] >= -20:
                    ezca['SQZ-LO_SERVO_IN1EN'] = 1
                    time.sleep(0.1)
                    if abs(ezca['SQZ-LO_SERVO_FASTMON']) < 10 or abs(ezca['SQZ-LO_SERVO_SLOWMON'])<10: #use SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO instead
                        return True
                    else:
                        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
                        ezca['SQZ-LO_SERVO_IN1EN'] = 0
                        notify('LO servo railed, bDiv closed')
                        self.fail_flag = True
                else:
                    notify('beam diverter is open but no 3MHz on OMC')
            
        




class LOCKED_OMC(GuardState):
    index = 20

    def main(self):
        servo_gain = 0
        control_chan = 'SQZ-OPO_PZT_1_OFFSET'
        readback_chan = 'SQZ-LO_SERVO_SLOWMON'
        self.servo = cdu.Servo(ezca, control_chan, readback=readback_chan,
                               gain=servo_gain)

    def run(self):
        self.servo.step() 
        #Look at transfer function, make sure the gain is good before turning on boosts
        #ezca['SQZ-LO_SERVO_COMBOOST']=1
        #ezca['SQZ-LO_SERVO_SLOWBOOST']=1

        if ezca['GRD-SQZ_OPO_STATE_N'] == 18:
            log('EOM rails')
            ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
            return 'LOCKING_OMC'
        if ezca['SQZ-OMC_TRANS_RF3_DEMOD_RFMON'] < -20:
            notify('lost RF3 MHz, close bDiv')
            ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
            #return 'ADJUST_FREQUENCY'
        return True





##################################################

edges = [
    ('INIT', 'DOWN'),
    ('IDLE', 'DOWN'),
    ('DOWN', 'ADJUST_FREQUENCY'),
    ('ADJUST_FREQUENCY','LOCKING_HD'),
    ('IDLE','LOCKING_HD'),
    ('LOCKING_HD', 'LOCKED_HD'),
    ('LOCKED_HD', 'DOWN'),
    ('ADJUST_FREQUENCY','LOCKING_OMC'),
    ('IDLE','LOCKING_OMC'),
    ('LOCKING_OMC', 'LOCKED_OMC'),
    ('LOCKED_OMC', 'DOWN')
]


